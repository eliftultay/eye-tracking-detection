from imutils import face_utils
from matplotlib import pyplot as plt
import imutils
import numpy as np
import cv2
import dlib
import json
import os
import shutil
import math

def rightEyeCrop(image, coordinates):
    xi, yi, xo, yo = 0
    rightEyeIn = []
    rightEyeOut = []
    images = []
    for i in range(len(coordinates)):
        if (i % 5 == 3):
            rightEyeIn.append(coordinates[i])
        if (i % 5 == 2):
            rightEyeOut.append(coordinates[i])

    for m in rightEyeIn:
        for n in rightEyeOut:
            xi, yi = m
            xo, yo = n
        #cropped = image[:, :]


    # Equalize Histogram
    gray = cv2.equalizeHist(image)
    cl1 = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    clahe = cl1.apply(gray)
    blur = cv2.medianBlur(clahe, 7.0)

    return images

def leftEyeCrop(image, coordinates):
    leftEyeIn = []
    leftEyeOut = []
    for i in range(len(coordinates)):
        if (i % 5 == 0):
            leftEyeOut.append(coordinates[i])

        if (i % 5 == 1):
            leftEyeIn.append(coordinates[i])
    return leftEyeIn

def check_path():

    try:

        if os.path.isfile('coordinates.txt'):
            os.remove('coordinates.txt')
        else:
            print('File NOT exist.\n')

        if os.path.exists('/Users/eliftultay/Desktop/Sample/Crop'):
            shutil.rmtree('/Users/eliftultay/Desktop/Sample/Crop')
            print("Path refreshed!\n")
        else:
            print('Directory NOT exist.\n')

        os.mkdir('/Users/eliftultay/Desktop/Sample/Crop')

    except OSError:
        pass

def eye_landmark(video):

    check_path()

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('shape_predictor_5_face_landmarks.dat')
    print("Detector & Predictor created. \n")

    cap = cv2.VideoCapture(video)
    print("Video taken. \n")
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) -1
    print("Total frame number: %d \n" % frame_count)
    fps = cap.get(cv2.CAP_PROP_FPS)
    duration = frame_count/fps
    print("Duration:", duration)

    counter = 0
    coordinates = []
    rightEyeCenters = []
    leftEyeCenters = []

    while cap.isOpened():

        ret, image = cap.read()
        image = imutils.rotate(image, -90)

        if ret == True:

            counter += 1
            print(image.shape)
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            rects = detector(gray, 1)

            for (i, rect) in enumerate(rects):

                # Face Predictor
                shape = predictor(gray, rect)
                shape = face_utils.shape_to_np(shape)
                print("Counter: %d \n" % counter)

                #Add eye region coordinates to list
                for (x,y) in shape:
                    coordinates.append([x, y])


                rightEyeCrop(gray, coordinates)




                #Convert Gray Image to BGR
                bgr = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

                #Draw points on eye edges
                for (x, y) in shape:
                    cv2.circle(bgr, (x, y), 1, (0, 255, 0), 10)

                # Hough Circle Detection
                circles = cv2.HoughCircles(blur, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=47,
                                           maxRadius=60)

                #Circle Not Detected
                if circles is not None:
                    circles = np.round(circles[0,:]).astype("int")
                    cLen = len(circles)
                    if (cLen == 2):

                        for j in range(cLen):

                            if (j == 0):

                                (x1, y1, r) = circles[0]
                                rightEyeCenters.append([x1, y1])

                            elif (j == 1):

                                (x1, y1, r) = circles[1]
                                leftEyeCenters.append([x1, y1])

                    elif(cLen == 1):
                        (x1, y1, r) = circles[0]
                        leftEyeCenters.append([x1, y1])
                        rightEyeCenters.append([x1, y1])

                else:
                    leftEyeCenters.append([-100, -100])
                    rightEyeCenters.append([-100, -100])

                if counter > (frame_count-1):
                    print("Cooridnates:", len(coordinates))
                    print("RE:", len(leftEyeCenters))
                    print("LE:", len(rightEyeCenters))
                    cap.release()
                    print( "Released")
                    break
        else:
            break

videoPath = 'm1.mp4'
eye_landmark(videoPath)
