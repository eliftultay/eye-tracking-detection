from imutils import face_utils
from matplotlib import pyplot as plt
import imutils
import numpy as np
import cv2
import dlib
import json
import os
import shutil
import math

def check_path():

    try:

        if os.path.isfile('coordinates.txt'):
            os.remove('coordinates.txt')
        else:
            print('File NOT exist.\n')

        if os.path.exists('/Users/eliftultay/Desktop/landmark'):
            shutil.rmtree('/Users/eliftultay/Desktop/landmark')
            print("Path refreshed!\n")
        else:
            print('Directory NOT exist.\n')

        os.mkdir('/Users/eliftultay/Desktop/landmark')

    except OSError:
        pass

def projection(xv, yv, xu, yu):

    print("PROJECTION CALCULATION\n")
    dist = math.sqrt(pow(xv, 2) + pow(yv, 2))
    dot = xu*xv + yu*yv
    xp = (dot/dist) * xv
    yp = (dot / dist) * yv
    proj = math.sqrt(pow(xp, 2) + pow(yp, 2))
    return (proj, dist)

def drawRightGraph(l = []):

    print("DRAW GRAPH\n")
    plt.plot(l)
    plt.ylabel('Distance')
    plt.xlabel('Frame Number')
    plt.title("Right Eye Result")
    plt.show()

def drawLeftGraph(l = []):

    print("DRAW GRAPH\n")

    plt.plot(l)
    plt.ylabel('Distance')
    plt.xlabel('Frame Number')
    plt.title("Left Eye Result")
    plt.show()

def leftEyeProjection(coord = [], leftCenter = []):

    print("LEFT EYE PROJECTION\n")
    total = int(len(coord)/5)
    leftEye = []
    leftEye1 = []
    result = []

    for i in range(total):

        if (i % 5 == 0):
            leftEye.append(coord[i])
            leftEye1.append(coord[i + 1])

    for i in range(total):

        (x1, y1) = leftEye[i]
        (x2, y2) = leftEye1[i]
        (xc, yc) = leftCenter[i]
        xv = x2 - x1  # x coordinate of the vector which take projection on it
        yv = y2 - y1  # y coordinate of the vector which take projection on it
        xu = xc - x2  # x coordinate of the vector which projected
        yu = yc - y2  # y coordinate of the vector which  projected
        result.append(projection(xv, yv, xu, yu))
        leftEyeComparison(result)

def rightEyeProjection(coord = [], rightCenter = []):

    print("RIGHT EYE PROJECTION\n")
    total = int(len(coord)/5)
    rightEye = []
    rightEye1 = []
    result = []

    for i in range(total):

        if (i % 5 == 2):
            rightEye.append(coord[i])
            rightEye1.append(coord[i+1])

    for i in range (total):
        (x1, y1) = rightEye[i]
        (x2, y2) = rightEye1[i]
        (xc, yc) = rightCenter[i]
        xv = x2 - x1  # x coordinate of the vector which take projection on it
        yv = y2 - y1  # y coordinate of the vector which take projection on it
        xu = xc - x2  # x coordinate of the vector which projected
        yu = yc - y2  # y coordinate of the vector which  projected
        result.append(projection(xv, yv, xu, yu))
        rightEyeComparison(result)

def leftEyeComparison(result = []):

    print("LEFT EYE COMPARISON\n")
    length = len(result)
    leftList = []

    for i in range(length):
        (proj, dist) = result[i]
        difference = dist
        leftList.append(difference)

    print("Length : ", len(leftList))
    drawLeftGraph(leftList)

def rightEyeComparison(result = []):

    print("RIGHT EYE COMPARISON\n")
    length = len(result)
    rightList = []


    for i in range(length):
        (proj, dist) = result[i]
        difference = dist - proj
        rightList.append(difference)

    drawRightGraph(rightList)

def eye_landmark(video, limit, gSize, ks):

    check_path()

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('shape_predictor_5_face_landmarks.dat')
    print("Detector & Predictor created. \n")

    cap = cv2.VideoCapture(video)
    print("Video taken. \n")
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) -1
    print("Total frame number: %d \n" % frame_count)
    fps = cap.get(cv2.CAP_PROP_FPS)
    duration = frame_count/fps
    print("Duration:", duration)

    counter = 0
    coordinates = []
    rightEyeCenters = []
    leftEyeCenters = []

    while cap.isOpened():

        ret, image = cap.read()
        image = imutils.rotate(image, -90)

        if ret == True:

            counter += 1
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            rects = detector(gray, 1)

            for (i, rect) in enumerate(rects):

                gray = cv2.equalizeHist(gray)
                cl1 = cv2.createCLAHE(clipLimit=limit, tileGridSize=gSize)
                clahe = cl1.apply(gray)
                blur = cv2.medianBlur(clahe, ks)

                shape = predictor(gray, rect)
                shape = face_utils.shape_to_np(shape)
                print("Counter: %d \n" % counter)

                bgr = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

                for (x, y) in shape:

                    cv2.circle(bgr, (x, y), 1, (0, 255, 0), 10)
                    coordinates.append([x, y])

                circles = cv2.HoughCircles(blur, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=47,
                                           maxRadius=60)  # houghcircles

                if circles is not None:

                    circles = np.round(circles[0, :]).astype("int")
                    cLen = len(circles)

                    if (cLen == 2):

                        for j in range(cLen):

                            if (j == 0):

                                (x1, y1, r) = circles[0]
                                rightEyeCenters.append([x1, y1])

                            elif (j == 1):

                                (x1, y1, r) = circles[1]
                                leftEyeCenters.append([x1, y1])

                    elif(cLen == 1):
                        (x1, y1, r) = circles[0]
                        leftEyeCenters.append([x1, y1])
                        rightEyeCenters.append([x1, y1])

                else:
                    leftEyeCenters.append([-100, -100])
                    rightEyeCenters.append([-100, -100])

                if counter > (frame_count-1):
                    print("Cooridnates:", len(coordinates))
                    print("RE:", len(leftEyeCenters))
                    print("LE:", len(rightEyeCenters))
                    rightEyeProjection(coordinates, rightEyeCenters)
                    leftEyeProjection(coordinates, leftEyeCenters)
                    cap.release()
                    print( "Released")
                    break
        else:
            break

videoPath = 'm1.mp4'
cLimit = 2.0
cGridSize = (8, 8)
mKsize = 7

eye_landmark(videoPath, cLimit, cGridSize, mKsize)





