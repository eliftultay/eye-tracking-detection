import cv2
import numpy as np
import os
import shutil
import time

def videoThresholding(inputDir, outputDir):

    try:
        if os.path.exists(outputDir):
            shutil.rmtree(outputDir)
        os.mkdir(outputDir)
    except OSError:
        pass

    startTime = time.time()
    print('Processing time started')
    videoCapture = cv2.VideoCapture(inputDir)
    print('Video read from input directory.')
    frameLength = int(videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)) -1  # number of frames in video
    print("Number of frames: \n", frameLength)
    frameCount = 0  # counter for frames
    print("Video Converting on process...\n")

    #For creating video
    frameWidth = int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    #videoOutput = cv2.VideoWriter('/Users/eliftultay/Desktop/Sample/Threshold/t5.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frameWidth, frameHeight), False)

    while videoCapture.isOpened():

        ret, image = videoCapture.read()

        if ret == True:

            frameCount = frameCount + 1
            blur = cv2.GaussianBlur(image, (5,5), 0)
            gray = cv2.cvtColor(blur, cv2.COLOR_RGB2GRAY)
            # converting video color to gray
            #ret, threshold = cv2.threshold(gray, 50, 255,cv2.THRESH_BINARY)

            for i in range (gray.shape[0]):
                for j in range(gray.shape[1]):
                    if gray[i][j] <= 60:
                        gray[i][j] = 0
                    else:
                        gray[i][j] = 255

            bgr = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

            circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=20, minRadius=50,
                                       maxRadius=60)
            if circles is not None:

                print("Yes Circle!!!!")
                circles = np.uint16(np.around(circles))

                for i in circles[0, :]:

                    cv2.circle(bgr, (i[0], i[1]), i[2], (0, 255, 0), 2)  # Outer circle in the image
                    cv2.circle(bgr, (i[0], i[1]), 2, (0, 255, 0), 3)  # inner circle center
                    print("inner outer circle draw")

                cv2.imwrite(outputDir + "/%d.jpg" % (frameCount), bgr)  # Saving frame to the output directory
            else:
                print('No Circle!!!!!!!!!')
                cv2.imwrite(outputDir + "/%d.jpg" % (frameCount), bgr)  # Saving frame to the output directory


            #videoOutput.write(gray)

            if frameCount > (frameLength-1):
                endTime = time.time()
                videoCapture.release()
                #videoOutput.release()
                print('Creating video from threshold frames completed.')
                print('Video convertion took' , endTime-startTime, 'seconds')
                break
        else:
            break

input = '/Users/eliftultay/Desktop/Sample/elif.mp4'
output = '/Users/eliftultay/Desktop/Threshold'
videoThresholding(input, output)