import cv2
import numpy as np
import os
import shutil
import time

def videoThresholding(inputDir, outputDir):

    try:
        if os.path.exists(outputDir):
            shutil.rmtree(outputDir)
        os.mkdir(outputDir)
    except OSError:
        pass

    startTime = time.time()
    print('Processing time started')
    videoCapture = cv2.VideoCapture(inputDir)
    print('Video read from input directory.')
    frameLength = int(videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)) -1  # number of frames in video
    print("Number of frames: \n", frameLength)
    frameCount = 0  # counter for frames
    print("Video Converting on process...\n")

    #For creating video
    frameWidth = int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    videoOutput = cv2.VideoWriter('/Users/eliftultay/Desktop/Sample/Threshold/t1.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frameWidth, frameHeight), False)

    while videoCapture.isOpened():

        ret, image = videoCapture.read()

        if ret == True:

            frameCount = frameCount + 1
            blur = cv2.GaussianBlur(image, (5,5), 0)
            gray = cv2.cvtColor(blur, cv2.COLOR_RGB2GRAY)
            ret, threshold = cv2.threshold(gray, 50, 255,cv2.THRESH_BINARY)
            if threshold is not None:
               cv2.imwrite(outputDir + "/%d.jpg" % (frameCount), threshold)  # Saving frame to the output directory
            else:
                cv2.imwrite(outputDir + "/%d.jpg" % (frameCount), threshold)  # Saving frame to the output directory

            print('Thresholding operation completed.')
            videoOutput.write(threshold)

            if frameCount > (frameLength-1):
                endTime = time.time()
                videoCapture.release()
                videoOutput.release()
                print('Creating video from threshold frames completed.')
                print('Video convertion took' , endTime-startTime, 'seconds')
                break
        else:
            break

input = '/Users/eliftultay/Desktop/Sample/'
output = '/Users/eliftultay/Desktop/Threshold'
videoThresholding(input, output)