import numpy as np
import cv2
import shutil
import os
import time

def frameSplitter(inputDir, outputDir):

    try:
        removeFolder(outputDir)
        os.mkdir(outputDir)
    except OSError:
        pass

    videoCapture = cv2.VideoCapture(inputDir)
    frameLenght = int(videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)) -1
    frameCounter = 0
    frameWidth = int(videoCapture.get(3))
    frameHeight = int(videoCapture.get(4))
    videoMaker = cv2.VideoWriter('/Users/eliftultay/Desktop/Sample/Edge/edge30x70.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frameWidth, frameHeight), False)

    print('Number of frames: \n', frameLenght)
    print('Edge detection process is beginning now. \n')

    while videoCapture.isOpened():

        ret, image = videoCapture.read()

        gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)  # remove niose from the image
        #median = cv2.medianBlur(gray,5)
        canny = cv2.Canny(blurred, 30, 70)  # Canny Edge Detection
        #canny = autoCanny(blurred)
        frameCounter = frameCounter + 1

        cv2.imwrite(outputDir + "/%d.jpg" % frameCounter, canny)
        print('image is writing to ')
        videoMaker.write(canny)

        if (frameCounter > (frameLenght - 1)):
            videoCapture.release()
            videoMaker.release()
            print('Creating video is finished. \n')
            break



def autoCanny(image, sigma= 0.33):

    v = np.median(image)
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    return edge

def removeFolder(outputDir):
    if os.path.exists(outputDir):
        shutil.rmtree(outputDir)

inputDirectory = '/Users/eliftultay/Desktop/deneme.mp4'
outputDirectory = '/Users/eliftultay/Desktop/Edges'
frameSplitter(inputDirectory, outputDirectory)