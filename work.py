import numpy as np
import cv2
import time
import os
import shutil

def convertVideoToFrame(inputDir, outputDir):

    try:
        # delete the files if directory exist
        if os.path.exists(outputDir):
            shutil.rmtree(outputDir)
        os.mkdir(outputDir) #recreates the directory again
    except OSError:
        pass

    startTime = time.time()
    videoCapture = cv2.VideoCapture(inputDir)

    #Creating video from frames
    frameWidth = int(videoCapture.get(3))
    frameHeight = int(videoCapture.get(4))
    vidOutput = cv2.VideoWriter('/Users/eliftultay/Desktop/hough_circles.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frameWidth, frameHeight))

    videoLength = int(videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)) -1 #number of frames in video
    print("Number of frames: \n", videoLength)
    count = 0 #counter for frames
    print("Video Converting on process...\n")

    while videoCapture.isOpened(): #Begins to detect the captures in video by frames

        ret, image = videoCapture.read()
        print("image capture opened")

        if ret == True:

            blurImage = cv2.medianBlur(image, 5)
            print("image blured")

            imageColorGray = cv2.cvtColor(blurImage, cv2.COLOR_RGB2GRAY)
            imageColorBGR = cv2.cvtColor(imageColorGray, cv2.COLOR_GRAY2BGR) #converting video color to gray

            print("gray scaled image\n")
            count = count + 1
            circles = cv2.HoughCircles(imageColorGray, cv2.HOUGH_GRADIENT, 1, 20, param1=71, param2=70, minRadius=0, maxRadius=0)
            if circles is not None:

                print("Hough Circle on each frame")
                circles = np.uint16(np.around(circles))
                for i in circles[0, :]:
                    cv2.circle(imageColorBGR, (i[0],i[1]), i[2], (0, 255, 0), 2) #Outer circle in the image
                    cv2.circle(imageColorBGR, (i[0],i[1]), 2, (0, 0, 255), 3) #inner circle center
                    print("inner outer circle draw")

                cv2.imwrite(outputDir + "/%d.jpg" % (count), imageColorBGR) #Saving frame to the output directory
            else :
                cv2.imwrite(outputDir + "/%d.jpg" % (count), imageColorBGR)  # Saving frame to the output directory

            print("image saved to directory")

            vidOutput.write(imageColorBGR)

            if(count > (videoLength-1)):
                endTime = time.time()
                videoCapture.release()
                vidOutput.release()
                print("Converting video took %d seconds." % (endTime-startTime))
                break
        else:
           break


inputDir = '/Users/eliftultay/Desktop/deneme.mp4'
outputDir = '/Users/eliftultay/Desktop/Sample/Frames'
convertVideoToFrame(inputDir,outputDir)
