import numpy as np
import cv2
import imutils
from matplotlib import pyplot as plt
import time

#cap = cv2.VideoCapture(0)  # initialize video capture
left_counter = 0  # counter for left movement
right_counter = 0  # counter for right movement
last_x = 0
last_y = 0
last_x_frame = 0
last_y_frame = 0

th_value = 5  # changeable threshold value

cap = cv2.VideoCapture('x.mp4')

def thresholding(x, x1):

    global last_x
    global last_x_frame
    global list
    global list1
    global frame_counter
    global point_counter
    global totalX

    frame_x_dif = x - last_x_frame
    x_dif = x1 - last_x

    last_x_frame = x
    last_x = x1

    totalX = x_dif - frame_x_dif

    if frame_counter%15 == 0:
        list.append(totalX)
        list1.append(point_counter)
        point_counter += 1


circle_counter = 0
eyes = cv2.CascadeClassifier('haarcascade_eye.xml')
list = []
list1 = []
point_counter = 0
frame_counter = 0
totalX = 0
video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) - 1

while frame_counter != video_length :
    ret, frame = cap.read()
    if frame.shape is not None:
        frame = imutils.rotate(frame, -90)
    cv2.line(frame, (320, 0), (320, 480), (0, 200, 0), 2)
    cv2.line(frame, (0, 200), (640, 200), (0, 200, 0), 2)
    if ret == True:
        col = frame

        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        pupilFrame = frame
        clahe = frame
        blur = frame
        edges = frame
        detected = eyes.detectMultiScale(frame, 1.3, 5)
        for (x, y, w, h) in detected:  # similar to face detection

            cv2.rectangle(frame, (x, y), ((x + w), (y + h)), (0, 0, 255), 1)  # draw rectangle around eyes
            cv2.line(frame, (x, y), (x + w, y + h), (0, 0, 255), 1)  # draw cross
            cv2.line(frame, (x + w, y), (x, y + h), (0, 0, 255), 1)
            pupilFrame = cv2.equalizeHist(
                frame[(int)(y + (h * .25)):(int)(y + h), x:(int)(x + w)])  # using histogram equalization of better image.
            cl1 = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))  # set grid size
            clahe = cl1.apply(pupilFrame)  # clahe
            blur = cv2.medianBlur(clahe, 7)  # median blur
            circles = cv2.HoughCircles(blur, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=48,
                                       maxRadius=57)  # houghcircles
            if circles is not None:  # if atleast 1 is detected
                circles = np.round(circles[0, :]).astype("int")  # change float to integer
                #print('integer', circles)
                found = False
                for (x1, y1, r) in circles:
                    cv2.circle(pupilFrame, (x1, y1), r, (0, 255, 255), 2)
                    cv2.rectangle(pupilFrame, (x1 - 5, y1 - 5), (x1 + 5, y1 + 5), (0, 128, 255), -1)
                    # set thresholds
                    if x1 != 0:
                        thresholding(x, x1)
                        circle_counter += 1
                        found = True
                        #print(detected)

                if not found and frame_counter%15 == 0:
                    list.append(totalX)
                    list1.append(point_counter)
                    point_counter += 1


        # frame = cv2.medianBlur(frame,5)
        cv2.imshow('image', pupilFrame)
        #cv2.imshow('clahe', clahe)
        #cv2.imshow('blur', blur)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    #print ( round(time.time() * 1000))
    frame_counter += 1


list[0] = 0

print('Frame Counter = ', frame_counter)
print('Circle Counter = ', circle_counter)

#print('X times = ', list) grafik verilerini tutuyor.

plt.plot(list1, list)
plt.ylabel('Left / Right')
plt.xlabel('sn / 4')
plt.show()

cap.release()
cv2.destroyAllWindows()