import numpy as np
import cv2
import os
import shutil

leftCounter = 0

rightCounter = 0

thresholdValue = 0

outputFile = open('coordinates.txt', 'w')

def imageProcessing():
    try:
        if os.path.exists('/Users/eliftultay/Desktop/result'):
            shutil.rmtree('/Users/eliftultay/Desktop/result')
        os.mkdir('/Users/eliftultay/Desktop/result')
    except OSError:
        pass

    eyes = cv2.CascadeClassifier("haarcascade_eye.xml")

    cap = cv2.VideoCapture('/Users/eliftultay/Desktop/e/%d.jpg')

    counter = 0

    getCount = 0

    setX, setY = 0

    set = False

    xAxis = []

    yAxis = []

    radius = []

    while cap.isOpened():
        ret, image = cap.read()

        print(counter)

        counter+=1

        if ret == True:

            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            detectedEye = eyes.detectMultiScale(gray, 1.3, 5)

            for (x, y, w, h) in detectedEye:

                cv2.rectangle(gray, (x,y), ((x+w), (y+h)), (0, 255, 0), 1) #draw rectangle around eyes

                pupilImage = cv2.equalizeHist(gray[(int)(y + (h * .25)):(int)(y + h), x:(int)(x + w)])

                clh = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))

                clahe = clh.apply(pupilImage)

                blur = cv2.medianBlur(clahe, 7)

                circles = cv2.HoughCircles(blur, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius= 55, maxRadius=70)

                if circles is not None:

                    circles = np.round(circles[0, :]).astype("int")
                    print("Circles found and changed float to integer.")

                    for (x, y, r) in circles:

                        cv2.circle(pupilImage, (x, y), r, (0, 255, 255), 2)

                        cv2.rectangle(pupilImage, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

                        threshold(x)

                        xAxis.append(x)

                        yAxis.append(y)

                        radius.append(r)

                        print("r: ", r)

                    cv2.imwrite("/Users/eliftultay/Desktop/result/%d.jpg" % (counter), pupilImage)

                else:

                    threshold(-1, -1, -1)

                    xAxis.append(-1)

                    yAxis.append(-1)

                    radius.append(-1)

                    cv2.imwrite("/Users/eliftultay/Desktop/result/%d.jpg" % (counter), pupilImage)
        else:
            cap.release()
            print("Operation Completed!")
            break



def threshold(value, setX, setY):

    global leftCounter

    global rightCounter



imageProcessing()
























